This project contains proof of concept code for reading and interpreting spatial information.

-visualizationsDemo/lidarVisionTest.py
Read a csv file containing a set of points and present them in a 3d environment.
Navigation in 3d is done with ASWD keys and the arrows keys.

cameraDepthEstimator/predictCamera.py
Use openCV to read the webcam images.
Process them with a tensorflow Neural Net to obtain depth information.
Display them on screen.

Missing: Invert the depth information to 3d space points, and then display them in a 3d environment.

